const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const typeDefs = require('./src/schema');
const resolvers = require('./src/resolver');
const mongoose = require('mongoose');

const Customers = require('./src/datasources/Customers');
const Celebrities = require('./src/datasources/Celebrities');
const Categories = require('./src/datasources/Categories');
const SubCategories = require('./src/datasources/SubCategories');
const Assets = require('./src/datasources/Assets');
const Services = require('./src/datasources/Services');
const Benefactors = require('./src/datasources/Benefactors');
const CelebServices = require('./src/datasources/CelebServices');
const Fans = require('./src/datasources/Fans');
const Reviews = require('./src/datasources/Reviews');
const Shouttys = require('./src/datasources/Shouttys');
const Occasions = require('./src/datasources/Occasions');

const Customer = require('./src/models/Customer');
const Celebrity = require('./src/models/Celebrity');
const Category = require('./src/models/Category');
const SubCategory = require('./src/models/Subcategory');
const Asset = require('./src/models/Asset');
const Service = require('./src/models/Service');
const Benefactor = require('./src/models/Benefactor');
const CelebService = require('./src/models/CelebService');
const Fan = require('./src/models/Fan');
const Review = require('./src/models/Review');
const Shoutty = require('./src/models/Shoutty');
const Occasion = require('./src/models/Occasion');


const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    playground: true,
    dataSources: () => {
        return {
            Customers: new Customers(Customer),
            Celebrities: new Celebrities(Celebrity),
            Categories: new Categories(Category),
            SubCategories: new SubCategories(SubCategory),
            Assets: new Assets(Asset),
            Services: new Services(Service),
            Benefactors: new Benefactors(Benefactor),
            CelebServices: new CelebServices(CelebService),
            Reviews: new Reviews(Review),
            Fans: new Fans(Fan),
            Shouttys: new Shouttys(Shoutty),
            Occasions: new Occasions(Occasion),
        }
    }
});

const app = express();
server.applyMiddleware({ app, bodyParserConfig: true });
app.use('/uploads', express.static('src/images'));

mongoose.connect('mongodb+srv://shoutty:Ishola4891@cluster0.d4fxa.mongodb.net/shoutty?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true }).then(client => {
    app.listen({ port: 4000 }, () =>
        console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
    );
}).catch(err => console.log(err)); 
