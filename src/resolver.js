const resolvers = {

    Mutation: {
        addCustomer: (root, { customerData }, { dataSources }, info) => {

            const newCustomer = JSON.parse(customerData);
            const customer = dataSources.Customers.addCustomer(newCustomer);

            return customer.then(customer => {
                return {
                    code: 200,
                    success: true,
                    message: 'Customer added successfully',
                    customer
                }
            }).catch(error => {
                return {
                    code: error.code,
                    success: false,
                    message: '',
                    customer: null
                }
            })
        },
        addCelebrity: (root, { celebData }, { dataSources }, info) => {

            const newCeleb = JSON.parse(celebData);
            const celeb = dataSources.Celebrities.addCelebrity(newCeleb);

            return celeb.then(celeb => {
                return {
                    code: 200,
                    success: true,
                    message: 'Celebrity added successfully',
                    celeb
                }
            }).catch(error => {
                return {
                    code: error.code,
                    success: false,
                    message: '',
                    celeb: null
                }
            })
        },
        addCategory: (root, { CategoryData }, { dataSources }, info) => {

            const newCategory = JSON.parse(CategoryData);
            const category = dataSources.Categories.addCategory(newCategory);

            return category.then(category => {
                return {
                    code: 200,
                    success: true,
                    message: 'Category added successfully',
                    category
                }
            }).catch(error => {
                return {
                    code: error.code,
                    success: false,
                    message: '',
                    category: null
                }
            })
        },
        addSubCategory: (root, { SubCategoryData }, { dataSources }, info) => {

            const newSubCategory = JSON.parse(SubCategoryData);
            const subcategory = dataSources.SubCategories.addSubCategory(newSubCategory);

            return subcategory.then(subcategory => {
                return {
                    code: 200,
                    success: true,
                    message: 'Sub Category added successfully',
                    subcategory
                }
            }).catch(error => {
                return {
                    code: error.code,
                    success: false,
                    message: '',
                    subcategory: null
                }
            })
        },
        addShoutty: (root, { ShouttyData }, { dataSources }, info) => {

            const newShoutty = JSON.parse(ShouttyData);
            const shoutty = dataSources.Shouttys.addSoutty(newShoutty);

            return shoutty.then(shoutty => {
                return {
                    code: 200,
                    success: true,
                    message: 'Shoutty added successfully',
                    shoutty
                }
            }).catch(error => {
                return {
                    code: error.code,
                    success: false,
                    message: '',
                    shoutty: null
                }
            })
        },

    },

    Query: {
        customers: async (root, args, { dataSources }, info) => {
            return dataSources.Customers.getCustomers();
        },
        customer: async (root, { customerId }, { dataSources }, info) => {
            let c = dataSources.Customers.getCustomer(customerId);
            return {
                customer: c
            }
        },
        shouttys: async (root, args, { dataSources }, info) => {
            return dataSources.Shouttys.getShouttys();
        },
        shoutty: async (root, { id }, { dataSources }, info) => {
            return dataSources.Shouttys.getShoutty(id);
        },
        celebs: async (root, args, { dataSources }, info) => {
            return dataSources.Celebrities.getCelebrities();
        },
        celeb: async (root, { celebId }, { dataSources }, info) => {
            return dataSources.Celebrities.getCelebrity(celebId);
        },
        categories: async (root, args, { dataSources }, info) => {
            return dataSources.Categories.getCategories();
        },
        subcategories: async (root, args, { dataSources }, info) => {
            return dataSources.SubCategories.getSubCategories();
        },
        assets: async (root, args, { dataSources }, info) => {
            return dataSources.Assets.getAssets();
        },
        services: async (root, args, { dataSources }, info) => {
            return dataSources.Services.getServices();
        },
        service: async (root, { serviceId }, { dataSources }, info) => {
            let c = dataSources.Services.getService(serviceId);
            return c;
        },
        category: async (root, { categoryId }, { dataSources }, info) => {
            let c = dataSources.Categories.getCategory(categoryId);
            return c;
        },
        subcategory: async (root, { subcategoryId }, { dataSources }, info) => {
            let c = dataSources.SubCategories.SubCategory(subcategoryId);
            return c;
        },
        occations: async (root, args, { dataSources }, info) => {
            return dataSources.Occasion.getOccasions();
        },
        occation: async (root, { occationId }, { dataSources }, info) => {
            return dataSources.Occasion.getOccasion(occationId);
        },
        reviews: async (root, args, { dataSources }, info) => {
            return dataSources.Review.getReviews();
        },
        benefactors: async (root, args, { dataSources }, info) => {
            return dataSources.Benefactor.getBenefactors();
        },
        benefactor: async (root, { benefactorId }, { dataSources }, info) => {
            return dataSources.Benefactor.getBenefactor(benefactorId);
        },








    },
    Celeb: {
        category: async ({ category }, args, { dataSources }, info) => {
            return dataSources.Categories.getCategory(category);
        },
        sub_category: async ({ sub_category }, args, { dataSources }, info) => {
            return dataSources.SubCategories.getSubCategory(sub_category);
        },
        benefactor: async ({ benefactor }, args, { dataSources }, info) => {
            return dataSources.Benefactors.getBenefactor(benefactor);
        },
        services: async ({ id }, args, { dataSources }, info) => {
            return dataSources.CelebServices.getService(id);
        },
        reviews: async ({ id }, args, { dataSources }, info) => {
            return dataSources.Reviews.getCelebReviews(id);
        },
        fans: async ({ id }, args, { dataSources }, info) => {
            return dataSources.Fans.getCelebFans(id);
        },
        shouttys: async ({ id }, args, { dataSources }, info) => {
            return dataSources.Shouttys.getCelebShoutty(id);
        },
    },
    Shoutty: {
        celeb: async ({ celeb }, args, { dataSources }, info) => {
            return dataSources.Celebrities.getCelebrity(celeb);
        },
        customer: async ({ customer }, args, { dataSources }, info) => {
            return dataSources.Customers.getCustomer(customer);
        },
        asset: async ({ asset }, args, { dataSources }, info) => {
            return dataSources.Assets.getAsset(asset);
        },
        occasion: async ({ occasion }, args, { dataSources }, info) => {
            return dataSources.Occasion.getOccasion(occasion);
        },
    },
    Fans: {
        celeb: async ({ celeb }, args, { dataSources }, info) => {
            return dataSources.Celebrities.getCelebrity(celeb);
        },
        customer: async ({ customer }, args, { dataSources }, info) => {
            return dataSources.Customers.getCustomer(customer);
        },
    }


}

module.exports = resolvers;