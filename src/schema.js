const { gql } = require('apollo-server-express');

const typeDefs = gql`
    
    "Category of celebrities based on industry they trive in"
    type Category {
        id: ID!
        "Category name"
        name: String!
        "permalink url of category"
        slug: String!
        "Collection of related subcategory"
        subcategory: [SubCategory]
        "Timestamp"
        createdAt: String!
    }

    type SubCategory {
        id: ID!
        "Sub category name"
        name: String!
        "permalink url of sub category"
        category: Category
        "permalink url of sub category"
        slug: String!
        "Timestamp"
        createdAt: String!

    }

    "Celeb and influencers schema"
    type Celeb {
        id: ID!
        "Where is celeb influencial - Sports, Musician etc"
        category:Category 
        "Extra details of celeb. e.g Bacelona Super star, Games of throne actor. sometimes people need to know the specifics of the celeb category"
        celeb_category_detail: String 
        sub_category:SubCategory
        slug: String!
        name: String!
        display_picture: String
        "couple of text desciption or sales line of the celeb for their profile page."
        payoff_line: String 
        "Every Celeb will power an award category in our empowerment scheme. This could be a non goverment or best graduating student of the year in universities. that is where we spend our 20% share"
        benefactor: Benefactor 
        display_name: String!
        email: String!
        mobile: String!
        "services include shoutout, join a meeting, post on my story, freestyle for me etc, celeb will select which service he can offer and set a price bla bla"
        services:[CelebService!] 
        fetured:Int
        is_active:Int
        reviews: [Reviews]
        fans: [Fans]
        "due to the shedule of celeb, a daily limit on order can be set, when met the order botton is disabled"
        daily_limit: Int 
        "related and previous shoutty completed by celeb"
        shouttys: [Shoutty] 
        "Timestamp"
        createdAt: String!
    }

    "A celebrity can have a fan base just like a follow. relationship is defined here"
    type Fans {
        celeb: Celeb!
        customer: [Customer!]
        "Timestamp"
        createdAt: String!
    }

    "This is about the celeb, customer, beneficiary and order details of shoutty"
    type Shoutty {
        "Influence to make shoutty"
        celeb: Celeb!
        "Customer ordering the shoutty"
        customer: Customer! 
        "If set indicate if shoutty is for teh customers or or ordering for someone else"
        iam_beneficiary: Int 
        "Order Tracking reference"
        order: String 
        "CDN url of Shoutty"
        asset: Asset 
        "is video accepted by the customer upon delivery?"
        is_accepted: Int 
        "is order completed?"
        is_completed: Int 
        order_date: String
        order_paid: Int
        "online payment reference"
        order_payment_reference: String 
        "Who is the Shoutty for"
        beneficiary_name: String 
        beneficiary_title: String 
        "What's something about the recipient you want influencer to know?"
        benficiary_about: String
        "What does the recipient love most about the Celeb? (optional)"
        benficiary_loves_about_celeb: String
        "who is ordering this Shoutty. typically the logged in user but editable"
        from_name: String 
        from_title: String 
        "from occasion list - could be birthday, wedding, anniversary etc"
        occasion: Occasion   
        "if set the Shoutty will not be displayed in celeb profile"
        hide_from_celeb_profile: Int 
        "Hide watermark on the video"
        hide_watermark: Int
        review: Review 
        "Timestamp"
        createdAt: String!
    }

    "could be birthday, wedding, anniversary etc A customer will select one when booking a shoutty"
    type Occasion {
        id: ID
        name: String!
        slug: String!
        "each occation has one or more field person to it. here we can have a json of fields and fields type Timestamp"
        required_data: String 
        createdAt: String!
    }

    "A NGO, award category, etc 20% we take from celebrity funds will be used to fund a category and celebrity must choose a benefactor"
    type Benefactor {
        id: ID
        name: String!
        slug: String!
        logo: String
        "Timestamp"
        createdAt: String!
    }

    "Customer review after purchase of a shoutty"
    type Reviews {
        id: ID
        "5 star rating of review"
        rating: String!
        "when the review was given"
        timeago: String!
        "The comment of the review"
        comment: String!
        "Related customer to is giving the review"
        customer: Customer!
        "Related customer to is giving the review"
        celeb: Celeb!
        "Timestamp"
        createdAt: String!
    }

    type Customer {
        id: ID
        title: String!
        slug: String!
        firstname: String!
        lastname: String!      
        email: String!
        mobile: String!
        password: String
        gender: Int
        "Timestamp"
        createdAt: String!
    }

    "A service e.g a shoutty, a meeting join, a story post etc"
    type Service {
        id: ID!
        "could be shoutout, join a call, office motivation, post on my status"
        name: String!
        "clean permalink url of the sercie"
        slug: String!
        "Timestamp"
        createdAt: String!
    }

    "We will be storing images and videos in a CDN like wasabi, here we will register them"
    type Asset {
        id: ID!
        "Owner could be root, celeb, customer"
        owner: String! 
        "Corresponding model id of the owner if any"
        owner_id: Int 
        "corresponding wasabi url"
        url: String! 
        "Timestamp"
        createdAt: String!
    }

    "Services provided by celeb and associated pricing and settings"
    type CelebService {
        id: ID!
        service: Service!
        price: Float!
        featured: Int
        celeb: Celeb!
        response_time: String!
        response_time_unit: String!
        "Timestamp"
        createdAt: String!
    }

    
    type CelebSingle {
        celeb: Celeb
        prev: Celeb
        next: Celeb
        related: [Celeb]
        "Timestamp"
        createdAt: String!
    }

    type CategorySingle {
        category: Category
        prev: Category
        next: Category
        related: [Category]
        celebs: [Celeb]
        "Timestamp"
        createdAt: String!
    }

    type SubCategorySingle {
        subcategory: SubCategory
        prev: SubCategory
        next: SubCategory
        related: [SubCategory]
        celebs: [Celeb]
        "Timestamp"
        createdAt: String!
    }

    type CustomerSingle {
        customer: Customer
        prev: Customer
        next: Customer
        related: [Customer]
        shoutty: [Shoutty]
        "Timestamp"
        createdAt: String!
    }

    type CelebResponse {
        data: [Celeb],
        totalCount: Int
        "Timestamp"
        createdAt: String!
    }

    type Post {
        slug: ID!
    }

    type BlogCategory {
        name: String!
        slug: ID!
    }

    type BlogPageResponse {
        data: [Post]
        categories: [BlogCategory]
    }

    type Query {
        services: [Service]
        service(serviceId: Int): Service
        categories: [Category]
        subcategories: [SubCategory]
        category(categoryId: Int): CategorySingle
        subcategory(subCategoryId: Int): SubCategorySingle
        customer(customerId: ID!): CustomerSingle
        customers: [Customer]
        celebs: [Celeb]
        celeb(slug: ID): Celeb
        #posts(page: String, category: String): BlogPageResponse
        #post(slug: String!): PostSingle
        assets: [Asset]
        asset(assetId: ID): [Asset]
        shouttys: [Shoutty]
        shoutty(shouttyId: ID): Shoutty
        fans(celeb: ID): [Fans]
        occations: [Occasion]
        occation: Occasion
        reviews: [Reviews]
        benefactors: [Benefactor]
        benefactor: Benefactor
    }

    type Mutation {
        addCustomer(customerData: String!): addCustomerResponse
        addCelebrity(celebData: String!): addCelebrityResponse
        addService(serviceData: String!): addServiceResponse
        addCategory(CategoryData: String!): addCategoryResponse
        addSubCategory(SubCategoryData: String!): addSubCategoryResponse

        addShoutty(ShouttyData: String!): addShouttyResponse
        addFan(FanData: String!): addFanResponse
        addReview(ReviewData: String!): addReviewResponse
    }

    type addFanResponse {
        code: Int!
        success: Boolean!
        message: String!
        fans: Fans
    }

    type addReviewResponse {
        code: Int!
        success: Boolean!
        message: String!
        reviews: Reviews
    }

    type addShouttyResponse {
        code: Int!
        success: Boolean!
        message: String!
        shoutty: Shoutty
    }

    type addCustomerResponse {
        code: Int!
        success: Boolean!
        message: String!
        customer: Customer
    }
    type addCelebrityResponse {
        code: Int!
        success: Boolean!
        message: String!
        celeb: Celeb
    }
    type addServiceResponse {
        code: Int!
        success: Boolean!
        message: String!
        service: Service
    }
    type addCategoryResponse {
        code: Int!
        success: Boolean!
        message: String!
        category: Category
    }
    type addSubCategoryResponse {
        code: Int!
        success: Boolean!
        message: String!
        subcategory: SubCategory
    }
`

module.exports = typeDefs;