const Celebrity = require('../models/Celebrity');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Celebrities extends MongoDataSource {
    getCelebrity(celebId) {
        return Celebrity.findById(celebId);
    }
    getCelebrities() {
        //return Celebrity.find().populate('category').populate('sub_category');
        return Celebrity.find();
    }
    async addCelebrity(newCeleb) {
        const c = new Celebrity(
            newCeleb
        );
        await c.save();
        return c.populate('category').populate('sub_category').execPopulate();
    }

}

module.exports = Celebrities;