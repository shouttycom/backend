const Category = require('../models/Category');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Categories extends MongoDataSource {
    getCategory(catId) {
        return Category.findById(catId)
    }
    getCategories() {
        return Category.find().populate('subcategory').exec();
    }
    async addCategory(newCategory) {
        const c = new Category(
            newCategory
        );
        await c.save();
        return c;
    }

}

module.exports = Categories;