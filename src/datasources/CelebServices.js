const CelebService = require('../models/CelebService');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class CelebServices extends MongoDataSource {
    getService(celebId) {
        return CelebService.find({
            celeb: celebId
        });
    }

}

module.exports = CelebServices;