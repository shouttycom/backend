const SubCategory = require('../models/Subcategory');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class SubCategories extends MongoDataSource {
    getSubCategory(id) {
        return SubCategory.findById(id);
    }
    getSubCategories() {
        return SubCategory.find();
    }
    async addSubCategory(newSubCategory) {
        const c = new SubCategory(
            newSubCategory
        );
        await c.save();
        return c;
    }

}

module.exports = SubCategories;