const Customer = require('../models/Customer');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Customers extends MongoDataSource {
    getCustomer(customerId) {
        return Customer.findById(customerId);
    }
    getCustomers() {
        return Customer.find();
    }
    async addCustomer(newCustomer) {
        const c = new Customer(
            newCustomer
        );
        await c.save();
        return c;
    }


}

module.exports = Customers;