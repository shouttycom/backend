const Fan = require('../models/Fan');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Fans extends MongoDataSource {
    getFan(Id) {
        return Fan.findById(Id)
    }
    getFans() {
        return Review.find();
    }
    async addFan(newFan) {
        const b = new Fan(
            newFan
        );
        await b.save();
        return b;
    }
    getCelebFans(celebId) {
        return Fans.find({
            celeb: celebId
        });
    }

}

module.exports = Fans;