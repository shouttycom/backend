const Service = require('../models/Service');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Services extends MongoDataSource {
    getService(servId) {
        return Service.findById(servId);
    }
    getServices() {
        return Service.find();
    }
}

module.exports = Services;