const Review = require('../models/Review');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Reviews extends MongoDataSource {
    getReview(Id) {
        return Review.findById(Id)
    }
    getReviews() {
        return Review.find();
    }
    async addReview(newReview) {
        const b = new Review(
            newReview
        );
        await b.save();
        return b;
    }

    getCelebReviews(celebId) {
        return Review.find({
            celeb: celebId
        });
    }
}

module.exports = Reviews;