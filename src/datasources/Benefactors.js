const Benefactor = require('../models/Benefactor');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Benefactors extends MongoDataSource {
    getBenefactor(Id) {
        return Benefactor.findById(Id)
    }
    getBenefactors() {
        return Benefactor.find();
    }
    async addBenefactor(newBenefactor) {
        const b = new Benefactor(
            newBenefactor
        );
        await b.save();
        return b;
    }

}

module.exports = Benefactors;