const Asset = require('../models/Asset');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Assets extends MongoDataSource {
    getAsset(assetId) {
        return Asset.findById(assetId);
    }
    getAssets() {
        return Asset.find();
    }

}

module.exports = Assets;