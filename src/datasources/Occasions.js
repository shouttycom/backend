const Occasion = require('../models/Occasion');
const { MongoDataSource } = require('apollo-datasource-mongodb');

class Occasions extends MongoDataSource {
    getOccasion(Id) {
        return Occasion.findById(Id)
    }
    getOccasions() {
        return Occasion.find();
    }
    async addOccasion(newOccasion) {
        const b = new Occasion(
            newOccasion
        );
        await b.save();
        return b;
    }

}

module.exports = Occasions;