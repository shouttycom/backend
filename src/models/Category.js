const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        required: true,
    },
}, { timestamps: true });

categorySchema.virtual('subcategory', {
    ref: 'SubCategory',
    localField: '_id',
    foreignField: 'category'
});


module.exports = mongoose.model('Category', categorySchema);