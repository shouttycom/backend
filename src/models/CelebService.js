const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const celebServiceSchema = new Schema({
    service: { type: Schema.ObjectId, ref: 'Service' },
    price: {
        type: Number,
        required: true,
    },
    featured: {
        type: Number,
    },
    celeb: { type: Schema.ObjectId, ref: 'Celebrity' },
    response_time: {
        type: Number,
        required: true,
    },
    response_time_unit: {
        type: String,
        required: true,
    },
}, { timestamps: true });


module.exports = mongoose.model('CelebService', celebServiceSchema);