const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const shouttySchema = new Schema({
    celeb: { type: Schema.ObjectId, ref: 'Celebrity' },
    customer: { type: Schema.ObjectId, ref: 'Customer' },
    iam_beneficiary: { type: Number, },
    order: { type: String, required: true, },
    asset: { type: Schema.ObjectId, ref: 'Asset' },
    is_accepted: { type: Number },
    is_completed: { type: Number },
    order_date: { type: String, required: true, },
    order_paid: { type: Number, required: true },
    order_payment_reference: { type: String, required: true, },
    beneficiary_name: { type: String },
    beneficiary_title: { type: String },
    benficiary_about: { type: String },
    benficiary_loves_about_celeb: { type: String },
    from_name: { type: String },
    from_title: { type: String },
    occasion: { type: Schema.ObjectId, ref: 'Occasion' },
    hide_from_celeb_profile: { type: Number },
    hide_watermark: { type: Number },
    review: { type: Schema.ObjectId, ref: 'Review' },

}, { timestamps: true });


module.exports = mongoose.model('Shoutty', shouttySchema);