const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const customerSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    mobile: {
        type: String,
        required: true,
    },
    password: {
        type: String,
    },
    profile_picture: {
        type: String,
    },
    gender: {
        type: Number,
    },
    celeb: { type: Schema.ObjectId, ref: 'Celebrity' },
}, { timestamps: true });


module.exports = mongoose.model('Customer', customerSchema);