const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const assetSchema = new Schema({
    owner: {
        type: String,
        required: true,
    },
    owner_id: {
        type: Number,
        required: true,
    },
    img_url: {
        type: String,
        required: true,
    },
}, { timestamps: true });


module.exports = mongoose.model('Asset', assetSchema);