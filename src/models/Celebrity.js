const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const celebritySchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    celeb_category_detail: {
        type: String,
    },
    payoff_line: {
        type: String,
    },
    benefactor: { type: Schema.ObjectId, ref: 'Benefactor' },

    daily_limit: {
        type: Number
    },
    slug: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    mobile: {
        type: String,
        required: true,
    },
    display_name: {
        type: String,
    },
    display_picture: {
        type: String,
    },
    password: {
        type: String,
    },
    profile_picture: {
        type: String,
    },
    category: { type: Schema.ObjectId, ref: 'Category' },
    sub_category: {
        type: Schema.ObjectId, ref: 'SubCategory',
    },
    response_time: {
        type: Number,
    },
    fatured: {
        type: Number,
    },
    is_active: {
        type: Number,
    },

}, { collection: 'celebrities' }, { timestamps: true });

celebritySchema.virtual('reviews', {
    ref: 'Review',
    localField: '_id',
    foreignField: 'celeb'
});

celebritySchema.virtual('fans', {
    ref: 'Fan',
    localField: '_id',
    foreignField: 'celeb'
});

celebritySchema.virtual('shouttys', {
    ref: 'Shoutty',
    localField: '_id',
    foreignField: 'celeb'
});

celebritySchema.virtual('services', {
    ref: 'CelebService',
    localField: '_id',
    foreignField: 'celeb'
});




module.exports = mongoose.model('Celebrity', celebritySchema);