const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const fanSchema = new Schema({
    celeb: { type: Schema.ObjectId, ref: 'Celebrity' },
    customer: { type: Schema.ObjectId, ref: 'Customer' },

}, { timestamps: true });


module.exports = mongoose.model('Fan', fanSchema);