const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const subcategorySchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        required: true,
    },
    category: {
        type: Schema.ObjectId, ref: 'Category',
    },
}, { collection: 'subcategories' }, { timestamps: true });

module.exports = mongoose.model('SubCategory', subcategorySchema);