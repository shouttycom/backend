const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const reviewSchema = new Schema({
    celeb: { type: Schema.ObjectId, ref: 'Celebrity' },
    customer: { type: Schema.ObjectId, ref: 'Customer' },
    timeago: { type: String, required: true, },
    comment: { type: String, },
    rating: { type: Number, required: true, },

}, { timestamps: true });


module.exports = mongoose.model('Review', reviewSchema);